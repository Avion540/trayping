# README #

Built in Visual Studio 2015.

### What is this repository for? ###

* TrayPing is a utility that shows your ping to either Google DNS or League of Legends NA server in your notification tray. You can drag TrayPing to your task bar to see your ping at a glance.

### How do I get set up? ###

* Download WinSparkle dll from this release page (Once you download the .zip, the dll will be in the Release folder) https://github.com/vslavik/winsparkle/releases
* Put WinSparkle dll everywhere TrayPing.exe is/will be

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* amnatek@gmail.com